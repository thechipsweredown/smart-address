import re

# get map of utf-8 character to telex
# example : đ - dd

def read_map_charater(file):
    dict_telex = {}
    with open(file) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    for i in range(len(content)):
        temp = content[i].split("-")
        dict_telex[temp[0]] = temp[1]
    return dict_telex

# get map of telex character to utf-8
# example : dd - đ

def read_map_telex(file):
    dict_telex = {}
    with open(file) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    for i in range(len(content)):
        temp = content[i].split("-")
        dict_telex[temp[1]] = temp[0]
    return dict_telex

# segment syll vietnamese
def syll_seg(word):
    dict_telex_tone = read_map_charater('telex_tone.txt')

    # extract syll + tone
    # example : huế -> huês
    for k, v in dict_telex_tone.items():
        if k in word:
            character, tone = v.split("_")[0], v.split("_")[1]
            word = word.replace(k, character)
            word = word + tone

    m = re.match(r'(b|c|d|đ|g|h|k|l|m|n|p|q|r|s|t|v|x|)(g|h|i|r|)(h|)(o|u|)(a|ă|â|e|ê|i|o|ô|ơ|u|ư|y)(a|ê|ô|ơ|)(c|i|m|n|o|p|t|u|y|)(h|g|)(f|s|r|x|j|)', word)
    if m == None:
        return '', '', '', '', ''

    syll_init, medial, princ_vowel, final_sound, tone = m.group(1) + m.group(2) + m.group(3), m.group(4), m.group(5) + m.group(6), m.group(7) + m.group(8), m.group(9)
    if medial == 'u' and ( princ_vowel == 'a' or princ_vowel == 'ô'):
        princ_vowel = medial + princ_vowel
        medial = ''
    if m.group(2) == 'i' and (m.group(5) == 'a' or m.group(5) == 'ê'):
        syll_init = m.group(1) + m.group(3)
        princ_vowel = 'i' + m.group(5)

    return syll_init, medial, princ_vowel, final_sound, tone

# normalize word wrong accent
# example : húê -> huế

def normalized_accent(word):

    map_telex = read_map_telex('telex_tone.txt')
    syll_init, medial, princ_vowel, final_sound, tone = syll_seg(word)
    word_accent = ""
    final_sound_arr = ['p', 't', 'c', 'ch', 'm', 'n', 'ng', 'nh', 'o', 'u', 'i']
    princ_arr = ['a', 'ă', 'â', 'e', 'ê', 'i', 'o', 'ô', 'ơ', 'u', 'ư', 'y']
    voewl_1 = ['iê', 'yê', 'uô', 'ươ']
    voewl_2 = ['ia', 'ya', 'ua', 'ưa']
    if len(princ_vowel) == 1:
        if medial == 'o' or medial == 'u':
            if tone != '':
                char_tone = princ_vowel + "_" + tone
                princ_vowel = map_telex[char_tone]
        elif medial != '' and final_sound == '':
            final_sound = princ_vowel
            princ_vowel = medial
            medial = ''
            if tone != '':
                char_tone = princ_vowel + "_" + tone
                princ_vowel = map_telex[char_tone]
        elif princ_vowel in princ_arr:
            char_tone = princ_vowel + "_" + tone
            princ_vowel = map_telex[char_tone]
        word_accent = syll_init + medial + princ_vowel + final_sound
    elif princ_vowel in voewl_1 and final_sound in final_sound_arr:
        first_voewl, second_voewl = princ_vowel[0], princ_vowel[1]
        if tone != '':
            char_tone = second_voewl + "_" + tone
            princ_vowel = first_voewl + map_telex[char_tone]
        word_accent = syll_init + medial + princ_vowel + final_sound
    elif princ_vowel == 'ia':
        if syll_init == 'g':
            first_voewl, second_voewl = princ_vowel[0], princ_vowel[1]
            if tone != '':
                char_tone = second_voewl + "_" + tone
                princ_vowel = first_voewl + map_telex[char_tone]
            word_accent = syll_init + medial + princ_vowel + final_sound
        else:
            first_voewl, second_voewl = princ_vowel[0], princ_vowel[1]
            if tone != '':
                char_tone = first_voewl + "_" + tone
                princ_vowel = map_telex[char_tone] + second_voewl
            word_accent = syll_init + medial + princ_vowel + final_sound
    elif princ_vowel == 'ua':
        if syll_init == 'q':
            first_voewl, second_voewl = princ_vowel[0], princ_vowel[1]
            if tone != '':
                char_tone = second_voewl + "_" + tone
                princ_vowel = first_voewl + map_telex[char_tone]
            word_accent = syll_init + medial + princ_vowel + final_sound
        else:
            first_voewl, second_voewl = princ_vowel[0], princ_vowel[1]
            if tone != '':
                char_tone = first_voewl + "_" + tone
                princ_vowel = map_telex[char_tone] + second_voewl
            word_accent = syll_init + medial + princ_vowel + final_sound
    elif princ_vowel in voewl_2:
        first_voewl, second_voewl = princ_vowel[0], princ_vowel[1]
        if tone != '':
            char_tone = first_voewl + "_" + tone
            princ_vowel = map_telex[char_tone] + second_voewl
        word_accent = syll_init + medial + princ_vowel + final_sound

    return word_accent

if __name__ == "__main__":
    print(normalized_accent("thai"))