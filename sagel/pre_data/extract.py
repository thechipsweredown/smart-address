import json
import unicodedata as ud
import unidecode
from sagel.pre_data import syll_process

def normalize(str):
    str = ud.normalize('NFC', str)
    # temp = []
    # for i in str.split(" "):
    #     temp.append(syll_process.normalized_accent(i))
    # return ' '.join(temp)
    return str

def remove_accents(str):
    return unidecode.unidecode(str)

def extractAddress(arr_adress):
    with open('./pre_data.json','a') as f:
        for i in range(len(arr_adress)):
            print(arr_adress[i]["name"].lower())
            name_city = normalize(arr_adress[i]["name"].lower()) #city
            district = arr_adress[i]["district"]
            for j in range(len(district)):
                name_dis = normalize(district[j]["name"].lower()) #district
                street = district[j]["street"] #street
                for k in range(len(street)):
                    str = {
                        "street": normalize(street[k]["name"].lower()).strip(),
                        "street_no": remove_accents(normalize(street[k]["name"].lower()).strip()),
                        "district": name_dis.strip(),
                        "district_no": remove_accents(name_dis.strip()),
                        "city": name_city.strip(),
                        "city_no" : remove_accents(name_city.strip()),
                        "code": arr_adress[i]["code"],
                        "country": "việt nam"
                    }
                    json.dump(str, f, ensure_ascii=False, separators=(',', ':'))
                    f.write("\n")
                ward = district[j]["ward"] #ward
                for n in range(len(ward)):
                    war = {
                        "ward": normalize(ward[n]["name"].strip().lower()).strip(),
                        "ward_no": remove_accents(normalize(ward[n]["name"].strip().lower()).strip()),
                        "district": name_dis.strip(),
                        "district_no": remove_accents(name_dis.strip()),
                        "city": name_city.strip(),
                        "city_no" : remove_accents(name_city.strip()),
                        "code": arr_adress[i]["code"],
                        "country": "việt nam"
                    }
                    json.dump(war, f, ensure_ascii=False, separators=(',', ':'))
                    f.write("\n")

                project = district[j]["project"] #project
                for m in range(len(project)):
                    pro = {
                        "project": normalize(project[m]["name"].lower()).strip(),
                        "project_no": remove_accents(normalize(project[m]["name"].lower()).strip()),
                        "district": name_dis.strip(),
                        "district_no": remove_accents(name_dis.strip()),
                        "city": name_city.strip(),
                        "city_no" : remove_accents(name_city.strip()),
                        "code": arr_adress[i]["code"],
                        "country": "việt nam",
                        "lat": project[m]["lat"],
                        "lng": project[m]["lng"]
                    }
                    json.dump(pro, f, ensure_ascii=False,separators=(',',':'))
                    f.write("\n")
if __name__ == "__main__":
    string1 = "bến"
    print(remove_accents(string1))