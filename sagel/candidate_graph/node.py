class Node:
    def __init__(self,value=None,type=None,childs=None,score=0,level=None,id=None,MCS=0):
        self.value = value
        self.type = type
        if childs == None:
            self.childs = []
        else:
            self.childs = childs
        self.score = score
        self.level = level
        self.id = id
        self.MCS = MCS

    def add_child(self,id):
        if id not in self.childs:
            self.childs.append(id)

    def assign(self,node_parent):
        self.value = node_parent.value
        self.type = node_parent.type
        self.childs = node_parent.childs
        self.score = node_parent.score
        self.level = node_parent.level
        self.id = node_parent.id
        self.MCS = node_parent.MCS

    def print(self):
        print(self.__dict__)

