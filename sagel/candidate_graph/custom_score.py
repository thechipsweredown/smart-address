# from sagel.candidate_graph.node import Node
#
#
# def count(string):
#     temp = string.replace(",","").replace("-","").split(" ")
#     map = {}
#     for t in temp:
#         if t in map.keys():
#             map.update({t : map.get(t) + 1})
#         else:
#             map.update({t : 1})
#     return map
#
#
# def c_score(address, node_parent, node_child):
#     map_address = count(address)
#     map_value = count(node_parent.value + " " + node_child.value)
#     arr_tuple = []
#     for k,v in map_address.items():
#         if k in map_value.keys():
#             t = []
#             t.append(map_address.get(k))
#             t.append(map_value.get(k))
#             arr_tuple.append(tuple(t))
#     c = 0
#     for tup in arr_tuple:
#         if tup[1] != 1 :
#             c += tup[1]/tup[0]
#     if c != 0:
#         return (node_parent.score + node_child.score)/c
#     else :
#         return node_parent.score + node_child.score
#
#
# def reduce_score(address, node_parent, node_child):
#     s = c_score(address, node_parent, node_child)
#     current_score = node_parent.score + node_child.score
#     if current_score != s and node_parent.level != 5:
#         node_child.score = s/2
#         node_parent.score = s/2
#     # if node_child.id == "nam phong|2":
#     #     print(node_parent.score)
#     #     print(node_child.score)
#
#
# def increase_score(address,value):
#     if value in address:
#         return 10
#     else:
#         return -10
#
#
# def test():
#     n1 = Node(value="nam định",score=13.5)
#     n2 = Node(value="nam định", score= 13.5)
#     print(c_score("nghĩa phong nam định", n1, n2))
#
#
# if __name__ == "__main__":
#     test()