from sagel.candidate_graph import visit_graph
from sagel.candidate_graph import top_k_results
import json
def normalize_address_cmd():
    address = input("Enter text : ")
    map = top_k_results.extract_address(address.strip())
    if map == None :
        print("Cannot extract address from your text !")
    else:
        json_addr = json.dumps(map, ensure_ascii=False, sort_keys=True, indent=4)
        print(json_addr)

def normalize_address(address):
    map = top_k_results.extract_address(address.lower().strip())
    if map == None :
        return "Cannot extract address from your text !"
    else:
        json_addr = json.dumps(map, ensure_ascii=False, sort_keys=True, indent=4)
        return json_addr

def main():
    try:
        while True:
            normalize_address_cmd()
    except:
        print('Cannot extract address from your text !')
if __name__ == "__main__":
    main()
