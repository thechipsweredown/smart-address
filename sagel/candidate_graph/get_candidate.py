from sagel.candidate_graph import query_elasticsearch

address = "Dự án Dabaco Vạn An, Đường Kinh Dương Vương, Xã Hòa Long, Bắc Ninh, Bắc Ninh"

def get_duplicate(addr, n):
    list_ngram = generate_ngrams(addr,n)
    dup = {}
    temp = []
    for i in list_ngram:
        if i in temp and i in dup.keys():
            dup.update({i : dup.get(i)+1})
        elif i in temp:
            dup.update({i : 1})
        else:
            temp.append(i)
    return dup

def generate_ngrams(s, n):
    s = s.lower()
    tokens = [token for token in s.split(" ") if token != ""]
    ngrams = zip(*[tokens[i:] for i in range(n)])
    return [" ".join(ngram) for ngram in ngrams]


def pre_process_query(addr):
    addr = addr.lower()
    addr = addr.replace(". "," ").replace("."," ")
    addr = addr.replace(", "," ").replace(","," ")
    max_gram = int(len(addr.split(" "))/2)
    for i in range(2, max_gram+1):
        duplicate = get_duplicate(addr,i)
        if len(duplicate.keys()) != 0:
            for dup_word in duplicate.keys():
                addr = addr.replace(dup_word,"",duplicate.get(dup_word))
    return addr.replace("  "," ",10)


def get_candidate(addr, field):
    addr = pre_process_query(addr)
    map_results = {}
    q = query_elasticsearch.build_query(addr, field)
    candidate = query_elasticsearch.match(q)
    map_results[field] = candidate
    return  map_results

if __name__ == "__main__":
    print(pre_process_query(address))
