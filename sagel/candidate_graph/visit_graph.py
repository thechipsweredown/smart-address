from sagel.candidate_graph import build_graph
from sagel.candidate_graph.stack import Stack
from sagel.candidate_graph.type_enum import Type

map_level = { 0 : "dự án/tòa nhà", 1 : "đường/phố", 2 : "xã/phường/thị trấn", 3: "quận/huyện/thị xã", 4 : "tỉnh/thành phố", 5: "quốc gia"}


def selection_sort(map, input_list):
    for idx in range(len(input_list)):
        min_idx = idx
        for j in range(idx + 1, len(input_list)):
            if map[input_list[min_idx]] < map[input_list[j]]:
                min_idx = j
        input_list[idx], input_list[min_idx] = input_list[min_idx], input_list[idx]


def copy_stack(stack_copy, stack):
    temp = []
    for i in stack.items:
        temp.append(i)
    stack_copy.items = temp


def visit_graph(graph, node, stack, result=[]):
    stack.push(node.id)
    childs = node.childs
    if len(childs) != 0:
        map = {}
        for child in childs:
            sum = graph.get(child).score + graph.get(child).MCS
            map.update({child: sum})
        selection_sort(map, childs)
        for j in childs:
            return visit_graph(graph, graph.get(j), stack, result)
    if node.type == Type.EXPLICIT:
        stack_copy = Stack()
        copy_stack(stack_copy, stack)
        result.append(stack_copy.items)
    stack.pop(0)
    return stack, result


def extract_address(addr):
    stack = Stack()
    result = []
    graph = build_graph.build(addr)
    if len(graph) == 0:
        return None
    else:
        node = graph["việt nam|5"]
        s, r = visit_graph(graph, node, stack, result)
        map = {}
        for i in r[0]:
            arr = i.split("|")
            map.update({map_level[int(arr[1])]: arr[0].title()})
        graph.clear()
        return map
