from sagel.candidate_graph.build_mini_graph import build_list_mini_candidate
from sagel.candidate_graph import  type_enum
from utils import sort
from ner.crf import ner_crf
import re


map_level = {0: "dự án/tòa nhà", 1:  "đường/phố/thôn", 2: "xã/phường/thị trấn", 3: "quận/huyện/thị xã", 4: "tỉnh/thành phố", 5: "quốc gia"}
map_ner = {0: "project", 1: "street", 2: "ward"}


def extract_number(number):
    arr = number.split(" ")
    length = len(arr)
    if length == 3:
        return arr[2] + "/" + arr[1] + "/" + arr[0]
    if length == 2:
        return arr[1] +"/"+ arr[0]
    else:
        return arr[0]

def extract_ner(addr):
    return ner_crf.detect_entity(addr)


def filter_address(addr):
    addr = ' '.join(addr.replace(",",", ").replace("-"," - ").split())
    addr = addr.replace("Thành phố","")
    addr = addr.replace("thành phố","")
    addr = addr.replace("Thành Phố","")
    addr = addr.replace("Thanh pho ","")
    addr = addr.replace("thanh pho ","")
    addr = addr.replace("Thanh Pho ","")
    isShort = re.search(r'(\.\w+)', addr)
    if isShort:
        addr = addr.replace(".", ". ")
    return addr


def normalize_result(ner_map,address):
    level = []
    map_res = {"normalize": {}, "maybe include": {}}
    map_res.update({"quốc gia": "Việt Nam"})
    for i in address.split(","):
        arr = i.strip().split("|")
        if len(arr) == 2:
            lv = int(arr[1])
            name = arr[0]
            level.append(lv)
            map_res.get("normalize").update({map_level[lv]: name.strip().title()})
    for i in range(0, 3):
        if i not in level:
            lv = map_ner.get(i)
            value = ner_map[lv]
            map_res.get("maybe include").update({map_level[i]: value.title()})
    map_res.get("maybe include").update({"số": extract_number(ner_map["number"])})
    map_res.get("maybe include").update({"tổ": ner_map["to"].title()})
    return  map_res

def remove_invalid_level(graph, address):
    # print(address)
    threshold_street = 5
    threshold_ward = 3
    threshold_district = 3
    threshold_city = 3

    arr_level = address.split(", ");
    if len(arr_level)==3:
        lv1 = arr_level[0]
        lv2 = arr_level[1]
        lv3 = arr_level[2]

        lv = int(lv1.split("|")[1])
        if lv == 1 :
            threshold = threshold_street
        else:
            threshold = threshold_ward
        # print(graph[lv1].score)
        if lv1 in graph and (graph[lv1].type == type_enum.Type.IMPLICIT or (graph[lv1].type == type_enum.Type.EXPLICIT and graph[lv1].score < threshold)):
            arr_level.remove(lv1)
        elif lv1 not in graph:
            arr_level.remove(lv1)
        if lv1 not in arr_level:
            if lv2 in graph and (graph[lv2].type == type_enum.Type.IMPLICIT or (graph[lv2].type == type_enum.Type.EXPLICIT and graph[lv2].score < threshold_district)):
                arr_level.remove(lv2)
                if lv3 in graph and (graph[lv3].type == type_enum.Type.IMPLICIT or (
                        graph[lv3].type == type_enum.Type.EXPLICIT and graph[lv3].score < threshold_city)):
                    arr_level.remove(lv3)
                elif lv3 not in graph:
                    arr_level.remove(lv3)
            elif lv2 not in graph:
                arr_level.remove(lv2)
                if lv3 in graph and (graph[lv3].type == type_enum.Type.IMPLICIT or (
                        graph[lv3].type == type_enum.Type.EXPLICIT and graph[lv3].score < threshold_city)):
                    arr_level.remove(lv3)
                elif lv3 not in graph:
                    arr_level.remove(lv3)
        return ', '.join(arr_level)
    else :
        return address


def extract_address(addr):
    addr_lower = addr.lower()
    addr_lower = filter_address(addr_lower)
    ner_map = extract_ner(addr)
    mini_map,priority,graph = build_list_mini_candidate(addr_lower,ner_map)
    mini_map = {key: val for key, val in mini_map.items() if val != 0.0}
    # print(addr)
    if len(priority) == 0:
        res = list(mini_map.keys())
        if len(res) == 0:
            return {"normalize": {}, "maybe include": {}}
        sort.selection_sort(mini_map, res)
        return normalize_result(ner_map,remove_invalid_level(graph,res[0]))
    else:
        map_temp_arr = []
        max = 0
        for k in mini_map.keys():
            list_temp = k.replace(", ",",").split(",")
            intersection = len(list(set(priority).intersection(list_temp)))
            if intersection == max:
                map_temp_arr.append({k: intersection})
                max = intersection
            elif intersection > max:
                map_temp_arr.clear()
                map_temp_arr.append({k : intersection})
                max = intersection
        if len(map_temp_arr) == 1:
            for k in map_temp_arr[0].keys():
                return normalize_result(ner_map,remove_invalid_level(graph,k))
        else:
            res =[]
            for k in map_temp_arr:
                for k1 in k.keys():
                    res.append(k1)
            sort.selection_sort(mini_map,res)
            return normalize_result(ner_map,remove_invalid_level(graph,res[0]))

def print_map(addr):
    ner_map = extract_ner(addr)
    print(ner_map)
    mini_map,_,_ = build_list_mini_candidate(addr, ner_map)
    for k, v in mini_map.items():
        print(k, v)

if __name__ == "__main__":
    print(extract_address("3. Chan ga rang muoi, sun ga rang muoi - So 151 Trieu Viet Vuong, Hai Ba Trung <3"))

