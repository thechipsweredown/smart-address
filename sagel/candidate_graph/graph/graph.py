from graphviz import Digraph
from sagel.candidate_graph import build_graph

addr = "lý thường kiệt hà nội"
build_graph.build_edges_list(addr)

g = Digraph('G',filename='graph/graph.gv')
g.attr(size='6,10')
g.node_attr.update(color='lightblue2', style='filled')
with open('graph/edges_list.txt','r') as f:
    for line in f:
        arr = line.split("=")
        g.edge(arr[0].rstrip(),arr[1].rstrip())

g.view()
