class Node(object):
    def __init__(self, value = None, childs=[]):
        self.value = value
        self.childs = childs
        print(id(childs))

    def add_child(self, str):
        self.childs.append(str)


def add(value, str):
    node = Node(value = value) #childs = []
    node.add_child(str)
    return node


node1 = add("ad", "1")
print(node1.childs)

node2 = add("daf","3")
print(node2.childs)
