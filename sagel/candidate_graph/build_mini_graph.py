from sagel.candidate_graph import build_graph
from utils import jascard,sort
from utils import viet_character
import unidecode

def update_top_k(graph, node, map_topK, k=5):
    list_childs = []
    childs = node.childs
    if len(childs) != 0:
        map_topK.update({node.id: {}})
        map = {}
        for child in childs:
            sum = graph.get(child).score + graph.get(child).MCS
            map.update({child: sum})
        sort.selection_sort(map, childs)
        for i in range(0,k):
            child = childs[i] if len(childs) > i else ""
            if child != "":
                list_childs.append(child)
                node_child = graph.get(child)
                map_topK[node.id].update({node_child.id : node_child.score})
    return list_childs


def get_top_k(graph, node, map_topK):
    list_childs = update_top_k(graph, node, map_topK)
    if len(list_childs) != 0:
        for i in list_childs:
            get_top_k(graph,graph[i], map_topK)


def extract_address_top_k(addr):
    map_topK = {}
    graph = build_graph.build(addr)
    if len(graph) == 0:
        return None
    else:
        node = graph["việt nam|5"]
        get_top_k(graph, node, map_topK)
    return map_topK,graph


def build_list_mini_candidate(addr, ner_map):
    mini_map = {}
    priority = []
    topK,graph = extract_address_top_k(addr)
    for k1, v1 in topK.get("việt nam|5").items():
        value = k1.split("|")[0]
        if ner_map["city"] == value:
            if k1 not in priority:
                priority.append(k1)
        if k1 in topK.keys():
            for k2, v2 in topK.get(k1).items():
                value = k2.split("|")[0]
                if ner_map["dist"] == value:
                    if k2 not in priority:
                        priority.append(k2)
                if k2 in topK.keys():
                    for k3, v3 in topK.get(k2).items():
                        value = k3.split("|")[0]
                        type = int(k3.split("|")[1])
                        if (ner_map["project"] == value and type == 0) or (ner_map["ward"] == value and type == 2) or (ner_map["street"] == value and type==1):
                            if k3 not in priority:
                                priority.append(k3)
                        res_str = k3.split("|")[0]+", "+k2.split("|")[0]+", "+k1.split("|")[0]

                        #contains Viet character
                        if viet_character.contains_Vietchar(addr) == False:
                            res_str = unidecode.unidecode(res_str)

                        jc = jascard.jaccard_similarity(addr, res_str) + jascard.c_score(addr, res_str)

                        mini_map.update({k3+", "+k2+", "+k1: jc})
                else:
                    res_str = k2.split("|")[0] + ", " + k1.split("|")[0]

                    # contains Viet character
                    if viet_character.contains_Vietchar(addr) == False:
                        res_str = unidecode.unidecode(res_str)

                    jc = jascard.jaccard_similarity(addr, res_str) + jascard.c_score(addr, res_str)

                    mini_map.update({k2 + ", " + k1: jc})


    return mini_map, priority,graph