from sagel.candidate_graph import get_candidate
from sagel.candidate_graph.type_enum import Type
from sagel.candidate_graph.node import Node

fields =["street", "ward", "district", "city"]
map_level = {"project" : 0, "street" : 1, "ward" : 2, "district" : 3, "city" : 4, "country" : 5}


def create_node_id(field, value):
    str_type = str(map_level[field])
    return value +"|"+str(str_type)


def check_parent(node):
    if node.get_level() > 5:
        return False
    else:
        return True


def get_field_parent(level_child):
    if  level_child == 0 or level_child == 1 or level_child ==2:
        return "district"
    if  level_child == 3:
        return "city"
    if  level_child == 4:
        return "country"
    else:
        return ""


def add_node(value, score, field, type, graph):
    node_id = create_node_id(field,value)
    node = graph.get(node_id,None)
    if node is None:
        level = map_level[field]
        node = Node(value=value,level=level,id=node_id)
    if type == Type.EXPLICIT:
        node.type = type
        node.score = score
    else:
        score = 0
        type = Type.IMPLICIT
        node.score = score
        node.type = type
    graph.update({node_id : node})
    return node


def add_child(node_parent, node_child):
    node_parent.add_child(node_child.id)
    new_mcs = node_child.score + node_child.MCS
    mcs = node_parent.MCS
    if new_mcs > mcs:
        node_parent.MCS = new_mcs


def build(addr):
    graph = {}
    for field in fields :
        candidate = get_candidate.get_candidate(addr, field)
        for j in range(len(candidate[field])):
            document = candidate[field][j]
            score = document["_score"]
            source = document["_source"]
            value = source[field]
            cnode = add_node(value, score, field, Type.EXPLICIT,graph)
            field_parent = get_field_parent(cnode.level)
            inode = Node(value=cnode.value, type = cnode.type, childs=cnode.childs, score = cnode.score, level=cnode.level, id = cnode.id, MCS=cnode.MCS)
            while field_parent != "":
                pnode = add_node(source[field_parent], 0, field_parent, Type.IMPLICIT,graph)
                add_child(pnode, inode)
                inode.assign(pnode)
                field_parent = get_field_parent(inode.level)
    return graph


def build_edges_list(addr):
    g = build(addr)
    with open('graph/edges_list.txt','w') as f:
        for k,v in g.items():
            childs = v.childs
            for i in childs:
                f.write(str(k)+"="+str(i))
                f.write("\n")


def print_graph(graph):
    for k,v in graph.items():
        print(k)
        print(v.print())
