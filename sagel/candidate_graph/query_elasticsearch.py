import json
import requests
import os
from utils import viet_character

host = "http://localhost:9200/"
try:
    host = os.environ['ELASTICSEARCH_HOST']
except:
    pass

uri = host+"smart_address/_search"


def build_query(address, field):
    if viet_character.contains_Vietchar(address) == False:
        field = field +"_no"

    query = json.dumps({
        "from": 0, "size": 1000,
        "query": {
                "bool": {
                  "must": {
                    "match": {
                      field: {
                        "query":address
                        # "fuzziness" : 2
                      }
                    }
                  },
                  "should": {
                    "match_phrase": {
                      field: {
                        "query": address,
                        "slop":  2
                      }
                    }
                  }
                }
      }
    })
    return query


def match(query):
    headers = {'Content-Type': 'application/json'}
    response = requests.get(uri, data=query, headers=headers)
    results = json.loads(response.text)
    return results["hits"]["hits"]


if __name__ == "__main__":
    print(viet_character.contains_Vietchar("hi hí"))
