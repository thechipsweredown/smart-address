from nltk import ngrams
import re

def get_ngrams(text, n):
    n_grams = ngrams(text, n)
    return [''.join(grams) for grams in n_grams]

def c_score(string1, string2):
    list2 = string2.split(", ")
    c = 0
    for i in list2:
        if i in string1:
            c += len(i.split(" "))
    return 0

def jaccard_similarity(string1, string2):
    sum = 0
    n_gram = 4
    count_word = 0
    for i in string2.split(", "):
        if i in string1:
            count_word += 1
    list1 = get_ngrams(re.sub(r'[^\w\s]', '', string1.lower()).strip(), n_gram);
    list2 = get_ngrams(re.sub(r'[^\w\s]', '', string2.lower()).strip(), n_gram);
    intersection = len(list(set(list1).intersection(list2)))
    union = (len(list1) + len(list2)) - intersection
    sum += float(intersection / union)
    return sum + float(0.1*count_word)


def main():
    string1 = "trung hòa thanh xuân".lower()
    string2 = "nhân hòa, thanh xuân, hà nội"
    string3 = "trung hòa, thanh xuân, hà nội"
    print(jaccard_similarity(string1, string2))
    print(jaccard_similarity(string1, string3))

if __name__ == "__main__":
    main()
