import unicodedata as ud


def normalize(str):
    return ud.normalize('NFC', str)

if __name__ == "__main__":
    print(normalize("bến thuỷ"))