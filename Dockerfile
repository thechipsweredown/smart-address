FROM ubuntu:16.04

RUN mkdir /smart-address

COPY . /smart-address

RUN apt-get update

WORKDIR /smart-address

RUN apt-get update -y \
    && apt-get install git -y \
    && apt install python3 -y \
    && apt install python3-pip -y \
    && apt install python3-venv -y \
    && python3 -m venv venv

RUN echo "source venv/bin/activate" > ~/.bash

RUN venv/bin/pip3 install --upgrade pip
RUN venv/bin/pip3 install --upgrade pip
RUN venv/bin/pip3 install git+https://www.github.com/keras-team/keras-contrib.git
RUN venv/bin/pip3 install -r /smart-address/requirements.txt

CMD venv/bin/python3 /smart-address/main.py

EXPOSE 5000
