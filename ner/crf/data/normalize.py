from utils import normalize_unicode

with open("test_v2.txt","w") as f:
    with open("test.txt","r") as f1:
        for line in f1:
            f.write(normalize_unicode.normalize(line))