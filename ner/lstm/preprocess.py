import csv
from setting import DATA_TRAIN_CRF
# write array data to csv file
def writeCsv(file, data):
    myFile = open(file, 'w')
    with myFile:
        writer = csv.writer(myFile)
        writer.writerows(data)
    myFile.close()

with open(DATA_TRAIN_CRF, 'r') as f:
    x = f.readlines()

data = [['Sentence #', 'Word', 'POS', 'Tag']]
sentence = 0
with open('count.txt','r') as f:
    sentence = int(f.readline())
sentence_str, flag = '', False
for i in range(len(x)):
    if x[i] == '\n':
        sentence = sentence + 1
        sentence_str = "Sentence " + str(sentence)
        flag = True
        if i>1:
            data_temp = ['', '.', '.', 'O']
            data.append(data_temp)
    else:
        row = x[i].split((' '))
        word, pos, tag = row[0], row[1], row[2].replace('\n', '')
        if sentence_str != '':
            data_temp = [sentence_str, word.lower(), pos, tag]
            sentence_str = ''
        else:
            data_temp = [sentence_str, word.lower(), pos, tag]
        data.append(data_temp)
with open('count.txt','w') as f:
    f.write(str(sentence))
writeCsv('data/test.csv', data)