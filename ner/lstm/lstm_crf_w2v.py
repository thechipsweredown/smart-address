from keras.preprocessing.sequence import pad_sequences
import pandas as pd
import numpy as np
from keras.models import Model, Input
from keras.layers import LSTM, Embedding, Dense, TimeDistributed, Bidirectional
from keras_contrib.layers import CRF
import pickle
from gensim.models import Word2Vec
from setting import MODEL_LSTM
data = pd.read_csv("data/train.csv", encoding="utf-8")
data = data.fillna(method="ffill")
words = []
tags = []

with open('data/words', 'rb') as f:
    words = pickle.load(f)
with open('data/tags', 'rb') as f:
    tags = pickle.load(f)
n_words = len(words)
n_tags = len(tags)
max_len=75

word_model = Word2Vec.load('model/word2vec.model')


def word2vec(word):
    try:
        return word_model.wv.vocab[word].index
    except:
        return 0


def idx2word(idx):
    return word_model.wv.index2word[idx]


pretrained_weights = word_model.wv.syn0
vocab_size, embedding_size = pretrained_weights.shape


def create_model():
    input = Input(shape=(max_len,))
    model = Embedding(input_dim=vocab_size, output_dim=embedding_size,
                      input_length=max_len, mask_zero=True)(input)  # 20-dim embedding
    model = Bidirectional(LSTM(units=embedding_size, return_sequences=True,
                               recurrent_dropout=0.1))(model)  # variational biLSTM
    model = TimeDistributed(Dense(50, activation="relu"))(model)  # a dense layer as suggested by neuralNer
    crf = CRF(n_tags)  # CRF layer
    output = crf(model)  # output

    model = Model(input,output)
    return model


model = create_model()
model.load_weights(MODEL_LSTM)
text="nghĩa phong, tỉnh nam định"
test_sentence = text.split(' ')
temp = []
for i in test_sentence:
    if "," not in i:
        temp.append(i)
    else:
        temp.append(i.replace(",", ""))
        temp.append(",")

x_test_sent = pad_sequences(sequences=[[word2vec(w) for w in temp]],
padding="post", value=0, maxlen=75)
p = model.predict(np.array([x_test_sent[0]]))
p = np.argmax(p, axis=-1)
print("{:15}||{}".format("Word", "Prediction"))
print(30 * "=")
for w, pred in zip(temp, p[0]):
    print("{:15}: {:5}".format(w, tags[pred]))