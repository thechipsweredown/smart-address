import gensim
from setting import MODEL_W2V,DATA_TRAIN_W2V

def load_data_train():
    documents = []
    with open(DATA_TRAIN_W2V,'r') as f:
        for line in f:
            line = line.replace('\n', '')
            documents.append(line)
    sentences = [[word for word in split_str(document.lower())] for document in documents]

    return sentences


def split_str(s):
    if ',' not in s:
        return s.split(" ")
    else:
        res = []
        arr = s.split(" ")
        for i in arr:
            if ',' in i:
                res.append(i.replace(",", ""))
                res.append(",")
            else:
                res.append(i)
        return res


def train_word2vec():
    sentences = load_data_train()
    word_model = gensim.models.Word2Vec(sentences, size=100, min_count=1,
                                        window=5, iter=100)
    word_model.save(MODEL_W2V)
    pretrained_weights = word_model.wv.syn0
    vocab_size, embedding_size = pretrained_weights.shape
    print("size vocab: ", vocab_size)
    print("size embedding: ", embedding_size)

    # def word2idx(word):
    #     return word_model.wv.vocab[word].index
    #
    # def idx2word(idx):
    #     return word_model.wv.index2word[idx]

    def word2vec(word):
        return word_model.wv.vocab[word].index


    def idx2word(idx):
        return word_model.wv.index2word[idx]


if __name__ == "__main__":
    train_word2vec()