from flask import *
from sagel.candidate_graph import top_k_results
app = Flask(__name__, static_url_path="/static")

@app.route('/')
def index():
    return render_template('index.html')

@app.route("/normalize", methods=['POST',"GET"])
def normalize():
    address = request.args.get('address')
    map = top_k_results.extract_address(address.strip())
    if map == None:
        return "Cannot extract address from your text !"
    else:
        json_addr = json.dumps(map, ensure_ascii=False, sort_keys=True, indent=4)
        return json_addr

def main():
    app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    app.run(host="0.0.0.0",port=5000,debug=True)