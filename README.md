# smart-address
##1. Setup project:
Clone project:
```
cd $HOME
git clone https://gitlab.com/thechipsweredown/smart-address
```

Install virtual environment:
```
sudo apt install virtualenv
```
Find excutable file python3:
```
which python3
```
Example: /usr/bin/python3

Create virtual environment for project
```
cd $HOME/smart-address
virtualenv -p /usr/bin/python3 venv
```

Update env of project:
```
source venv/bin/activate
```

Install library for project:
```
pip install -r requirements.txt
```
Install keras-contrib:
```
bin/install_keras_contrib.sh
```

##2. Install elasticsearch:
Run:
```
sudo sysctl -w vm.max_map_count=262144
```
Run :
```
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.6.2.tar.gz
```
Extract :
```
tar -xvzf elasticsearch*
```
Start elasticsearch:
```
elasticsearch*/bin/elasticsearch
```

Index data address:
```
cd $HOME/smart-address/sagel/create_index
```
And run:
```
./create_index_cmd2.sh
```

##3. Testing:
```
cd $HOME/smart-address/
python run_test.py
> Enter text : <press_your_address>
```

##OR run in docker to install Elasticsearch:

Clone project:
```
cd $HOME
git clone https://gitlab.com/thechipsweredown/smart-address
```

Build and run services in docker (smart-address and elasticsearch)
```
cd $HOME/smart-address/
./run_service_docker.sh
```
After services is started (on background, you can check port 9200 is opened?):<br/>
Index data address:

```
cd $HOME/smart-address/sagel/create_index
```
And run:
```
./create_index_cmd2.sh
```
Config virtual environment as 1. Setup project <br/>

In root project folder:
```
python run_test.py

> Enter text:
```

