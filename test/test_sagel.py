from sagel.candidate_graph import visit_graph
import json
import time
import unidecode
start = int(time.time())

rstreet = 763
rward = 285
rdistrict = 994
rcity = 997
n = 1001

pstreet = 0
pward = 0
pdistrict = 0
pcity = 0

count_acc = 0
count_street = 0
count_ward = 0
count_district = 0
count_city = 0

with open('test/normalize.txt','r') as f:
    for line in f:
        address = line.split("#")[0].strip()
        true_json = line.split("#")[1].strip()

        normalize_json = json.loads( true_json)
        true_street = ""
        true_ward = ""
        true_city = ""
        true_district = ""
        if "1" in normalize_json:
            true_street = normalize_json["1"]

        if  "2" in normalize_json:
            true_ward = normalize_json["2"]

        if "3" in normalize_json:
            true_district = normalize_json["3"]

        if "4" in normalize_json:
            true_city =  normalize_json["4"]

        test_street = ""
        test_ward = ""
        test_district = ""
        test_city = ""

        map = visit_graph.extract_address(address.strip())

        if "đường/phố" in map:
            test_street = map["đường/phố"]
            pstreet += 1

        if  "xã/phường/thị trấn" in map:
            test_ward = map["xã/phường/thị trấn"]
            pward += 1

        if "quận/huyện/thị xã" in map:
            test_district = map["quận/huyện/thị xã"]
            pdistrict += 1

        if "tỉnh/thành phố" in map:
            test_city =  map["tỉnh/thành phố"]
            pcity += 1

        if true_street == test_street and true_street != "":
            count_street += 1

        if true_ward == test_ward and true_ward != "":
            count_ward += 1

        if true_district == test_district and true_district != "":
            count_district += 1

        if true_city == test_city and true_city != "":
            count_city += 1

        if true_city == test_city :
            if true_district == test_district:
                if(true_street == "" and true_ward == ""):
                    count_acc += 1
                elif true_street != "" and true_street == test_street:
                    count_acc += 1
                elif true_ward != "" and true_ward == test_ward:
                    count_acc += 1


print("recall street : "+str(count_street/rstreet))
print("recall ward :"+str(count_ward/rward))
print("recall district : "+str(count_district/rdistrict))
print("recall city :"+str(count_city/rcity))

print("precision street : "+str(count_street/pstreet))
print("precision ward :"+str(count_ward/pward))
print("precision district : "+str(count_district/pdistrict))
print("precision city :"+str(count_city/pcity))

print("accuracy :"+str(count_acc/n))

# print(int(time.time()) - start)

#50s no edit distance
#85s with edit distance