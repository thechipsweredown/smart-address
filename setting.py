import os

DIR_PATH_DATA = os.path.dirname(os.path.realpath(__file__))

DATA_TRAIN_LSTM = DIR_PATH_DATA + '/ner/lstm/data/train.csv'
DATA_TEST_LSTM = DIR_PATH_DATA + '/ner/lstm/data/test.csv'
MODEL_LSTM = DIR_PATH_DATA + '/ner/lstm/model/lstm_crf.h5'

DATA_TRAIN_CRF = DIR_PATH_DATA + '/ner/crf/data/train_v2.txt'
DATA_TEST_CRF = DIR_PATH_DATA + '/ner/crf/data/test_v2.txt'
MODEL_CRF = DIR_PATH_DATA + '/ner/crf/model/crf.model'

DATA_TRAIN_W2V = DIR_PATH_DATA + '/ner/lstm/word2vec/data_word2vec.txt'
MODEL_W2V = DIR_PATH_DATA + '/ner/lstm/model/word2vec.model'
